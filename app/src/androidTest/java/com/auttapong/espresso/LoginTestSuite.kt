package com.auttapong.espresso

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.auttapong.espresso.login.LoginActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class LoginTestSuite {
    @get:Rule
    var mainActivityActivityTestRule: ActivityScenarioRule<LoginActivity> =
        ActivityScenarioRule(LoginActivity::class.java)

    private var tvLoginTitle = onView(withId(R.id.tv_login_title))
    private var etUsername = onView(withId(R.id.et_username))
    private var etPassword = onView(withId(R.id.et_password))
    private var bLogin = onView(withId(R.id.b_login))

    @Test
    fun loginSuccess() {
        val username = "username"
        val password = "password"

        etUsername.perform(ViewActions.typeText(username))
        etPassword.perform(ViewActions.typeText(password))
        bLogin.perform(click())

        tvLoginTitle.check(matches(withText("Success")))
    }

    @Test
    fun loginFail() {
        val username = "username"
        val password = "password2"

        etUsername.perform(ViewActions.typeText(username))
        etPassword.perform(ViewActions.typeText(password))
        bLogin.perform(click())

        tvLoginTitle.check(matches(withText("Fail")))
    }
}