package com.auttapong.espresso.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.auttapong.espresso.R
import com.auttapong.espresso.list.ListActivity

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val tvLoginTitle = findViewById<TextView>(R.id.tv_login_title)
        val bLogin = findViewById<Button>(R.id.b_login)
        val etUsername = findViewById<EditText>(R.id.et_username)
        val etPassword = findViewById<EditText>(R.id.et_password)

        bLogin.setOnClickListener {
            if (etUsername.text.toString() == "username" && etPassword.text.toString() == "password") {
                tvLoginTitle.text = "Success"
//                Handler().postDelayed({
//                    val intent = Intent(this, ListActivity::class.java)
//                    startActivity(intent)
//                }, 100)
            } else {
                tvLoginTitle.text = "Fail"
            }
        }
    }
}